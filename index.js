// console.log("Happy Thursday!")


/*

    Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

    Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

    Create a variable address with a value of an array containing details of an address.

*/

const num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);


/* Destructure the array and print out a message with the full address using Template Literals. */

const address = ["258 Washington Ave NW", "California", "90011"];
const [street, city, zipCode] = address;
console.log(`I live at ${street}, ${city} ${zipCode}`);

/*

    Create a variable animal with a value of an object data type with different animal details as it’s properties.

    Destructure the object and print out a message with the details of the animal using Template Literals.

*/

const animal = {
  species: "Saltwater Crocodile",
  name: "Lolong",
  weight: "1075 kgs",
  measurement: "20 ft 3in",
};
const { name, species, weight, measurement } = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

/*

    Create an array of numbers.

    Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

*/

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((num) => console.log(num));

/* Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array. */

const reduceNumber = numbers.reduce((sum, num) => sum + num);
console.log(reduceNumber);

/* Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties. */

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

/* Create/instantiate a new object from the class Dog and console log the object. */

const frankie = new Dog("Frankie", 5, "Miniature Dachhund");
console.log(frankie);

/* Create/Instantiate atleast 3 Dog objects for the last objective of the activity */

const nala = new Dog("Nala", 3, "Chow Chow");
const jiro = new Dog("Jiro", 2, "Shiba Inu");
const sasha = new Dog("Sasha", 4, "Samoyed");
